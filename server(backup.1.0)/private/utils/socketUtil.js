﻿var crypto = require('crypto')
	, dbUtil = require('./dbUtil')
	, mapUtil = require('./mapUtil')
	, commonUtil = require('./commonUtil');

var WEB_USER_TYPE = 1;

var socket = module.exports = function(app) {
	var userInfoMap = new Array();
	var io = require('socket.io').listen(app);
	
	io.configure(function() {
		io.set('log level', 3);
		io.set('transports', ['websocket', 'flashsocket', 'htmlfile', 'xhr-polling', 'jsonp-polling']);
	});
	
	io.of('/signupSocket').on('connection', function(socket) {
		console.log('회원가입 소켓 접속 성공');
		
		// 웹에서 회원 가입 시 발생하는 서버 이벤트
		socket.on('signup', function(req, callback) {
			var account = req.account;
			var pwd = req.pwd;
			
			var response = {};
			
			if (account && pwd) {
				//이미 존재하는 아이디인지 확인한다.
				dbUtil.isExistUserId(account, function(isExist) {
					//존재하는 아이디.
					if (isExist) {
						response.status = 202;
						callback(response);
						commonUtil.logPrint('[회원가입] 이미 가입된 계정, ' + account);
					}
					//존재하지 않는 아이디.
					else {
						//새로운 회원을 등록한다.
						dbUtil.registryNewUser(account, pwd, function(isSuccess) {
							//회원 가입 성공.
							if (isSuccess) {
								response.status = 200;
								callback(response);
								commonUtil.logPrint('[회원가입] 회원 가입 성공, ' + account);
							}
							//회원 가입 실패.
							else {
								response.status = 201;
								callback(response);
								commonUtil.logPrint('[회원가입] 회원 가입 실패, ' + account);
							}
						});
					}
				});
			}
			else {
				response.status = 201;
				callback(response);
				commonUtil.logPrint('[회원가입] 전달받은 값이 올바르지 않음, ' + account);
			}
		});
	});
	
	io.of('/loginSocket').on('connection', function(socket) {
		console.log('로그인 소켓 접속 성공');
		
		// 웹에서 로그인 시 발생하는 서버 이벤트
		socket.on('login', function(data, callback) {
			var account = data.account;
			var pwd = data.pwd;
			
			var response = {};
			
			if (account && pwd) {
				dbUtil.isExistUserId(account, function(isExist) {
					if (isExist) {
						//사용자 아이디와 비밀번호가 유효한지 확인한다.
						dbUtil.isValidUser(account, pwd, function(isValid) {
							//유효한 사용자 아이디와 패스워드.
							if (isValid) {
								var userData = mapUtil.getUserData(account);
								
								//등록된 로그인 정보가 없을 때, 로그인 정보를 생성한다.
								if (userData == null) {
									userData = new UserData();
								}
								
								//account, timestamp 값 이용하여 인증키 생성
								var sha256 = crypto.createHash('sha256');
								sha256.update(account + new Date().getTime().toString());
								var authKey = sha256.digest('hex');
								
								//account, 생성한 authKey를 저장한다.
								userData.addAuthKey(authKey);
								mapUtil.putUserData(account, userData);
								
								response.status = 100;
								response.data = authKey;
								callback(response);
								commonUtil.logPrint('[로그인] 로그인 성공, ' + account);
							}
							//올바르지 않은 패스워드.
							else {
								response.status = 102;
								response.data = null;
								callback(response);
								commonUtil.logPrint('[로그인] 올바르지 않은 비밀번호, ' + account);
							}
						});
					}
					//존재하지 않는 사용자 아이디.
					else {
						response.status = 101;
						response.data = null;
						callback(response);
						commonUtil.logPrint('[로그인] 올바르지 않은 계정, ' + account);
					}
				});
			}
			else {
				response.status = 101;
				response.data = null;
				callback(response);
				commonUtil.logPrint('[로그인] 전달받은 값이 올바르지 않음, ' + account);
			}
		});
		
	});
	
	io.of('/memoSocket').on('connection', function(socket) {
		console.log('메모 소켓 접속 성공');
	
		// 웹에서 메모 요청시 발생하는 서버 이벤트
		socket.on('requestMemos', function(data, callback) {
			console.log('requestMemos 호출');
			var account = data.account;
			var authKey = data.auth;
			var type = WEB_USER_TYPE;
			
			var response = {};
			
			var userData = mapUtil.getUserData(account);
			
			if (account && authKey && type) {
				//사용자의 정보가 존재함.
				if (userData) {
					//사용자의 정보에 일치하는 인증키가 존재함.
					if (userData.isExistAuthKey(authKey)) {
						//메모를 가져온다.
						dbUtil.getMemos(account, type, function(isSuccess, data) {
							//메모 가져오기 성공.
							if (isSuccess) {
								response.status = 340;
								response.data = data;
								commonUtil.logPrint('[메모 요청] 메모 목록 불러오기 성공, ' + account);
							}
							//메모 가져오기 실패.
							else {
								response.status = 341;
								response.data = null;
								commonUtil.logPrint('[메모 요청] 메모 목록 불러오기 실패, ' + account);
							}
							
							callback(response);
							
						});
					}
					//사용자의 정보에 일치하는 인증키가 존재하지 않음. -> 인증키가 유효하지 않음.
					else {
						response.status = 103;
						response.data = null;
						callback(response);
						commonUtil.logPrint('[메모 요청] 올바르지 않은 인증키, ' + account);
					}
				}
				//사용자의 정보가 존재하지 않음. -> 인증키가 유효하지 않음.
				else {
					response.status = 103;
					response.data = null;
					callback(response);
					commonUtil.logPrint('[메모 요청] 로그인 정보가 존재하지 않음, ' + account);
				}
			}
			else {
				response.status = 103;
				response.data = null;
				callback(response);
				commonUtil.logPrint('[메모 요청] 전달받은 값이 올바르지 않음, ' + account);
			}
		});

		// 웹에서 메모 작성시 발생하는 서버 이벤트
		socket.on('writeWebMemo', function(req, callback) {
			console.log('writeWebMemo 호출');
		
			var account = req.account;
			var authKey = req.auth;
			var type = WEB_USER_TYPE;
			var memo = req.data;
			var date = (new Date()).getTime();
			
			var response = {};
			
			var userData;
			
			if (account && authKey && type && memo && date) {
				userData =  mapUtil.getUserData(account);
				
				//사용자의 정보가 존재함.
				if (userData) {
					//사용자의 정보에 일치하는 인증키가 존재함.
					if (userData.isExistAuthKey(authKey)) {
						//새로운 메모를 쓴다.
						dbUtil.writeWebMemo(account, memo, date, function(isSuccess) {
							//메모 쓰기 성공.
							if (isSuccess) {
								//메모 목록을 가져온다.
								dbUtil.getMemos(account, type, function(isSuccess, data) {
									//메모 쓰기 성공, 메모 가져오기 성공.
									if (isSuccess) {
										response.status = 310;
										response.data = data;
										callback(response);
										commonUtil.logPrint('[메모 작성] 메모 작성 성공, 메모 목록 불러오기 성공(310), ' + account);
									}
									//메모 쓰기 성공, 메모 가져오기 실패.
									else {
										response.status = 311;
										response.data = null;
										callback(response);
										commonUtil.logPrint('[메모 작성] 메모 작성 성공, 메모 목록 불러오기 실패(311), ' + account);
									}
								});
							}
							//메모 쓰기 실패.
							else {
								response.status = 312;
								response.data = null;
								callback(response);
								commonUtil.logPrint('[메모 작성] 메모 작성 실패(312), ' + account);
							}
						});
					}
					//사용자의 정보에 일치하는 인증키가 존재하지 않음. -> 인증키가 유효하지 않음.
					else {
						response.status = 103;
						response.data = null;
						callback(response);
						commonUtil.logPrint('[메모 작성] 올바르지 않은 인증키(103), ' + account);
					}
				}
				//사용자의 정보가 존재하지 않음. -> 인증키가 유효하지 않음.
				else {
					response.status = 103;
					response.data = null;
					callback(response);
					commonUtil.logPrint('[메모 작성] 로그인 정보가 존재하지 않음(103), ' + account);
				}
			}
			else {
				response.status = 103;
				response.data = null;
				callback(response);
				commonUtil.logPrint('[메모 작성] 전달받은 값이 올바르지 않음, ' + account);
			}
		});
		
		// 웹에서 메모 삭제 시 발생하는 서버 이벤트
		socket.on('removeWebMemo', function(req, callback) {
			console.log('removeWebMemo 호출');
		
			var account = req.account;
			var authKey = req.auth;
			var type = WEB_USER_TYPE;
			var memoIds = req.data;
			
			var response = {};
			
			var userData;
			var successCnt = 0, tryCnt = 0;
			
			if (account && authKey && type && memoIds) {
				userData = mapUtil.getUserData(account)
				
				//사용자의 정보가 존재함.
				if (userData) {
					//사용자의 정보에 일치하는 인증키가 존재함.
					if (userData.isExistAuthKey(authKey)) {
						//전달받은 메모 목록이 존재하지 않으면, 서버에 저장된 메모 목록을 보낸다.
						if (memoIds.length == 0) {
							dbUtil.getMemos(account, type, function(isSuccess, data) {
								//메모 삭제 완료, 메모 가져오기 성공.
								if (isSuccess) {
									response.status = 320;
									response.data = data;
									response.tryCnt = tryCnt;
									response.successCnt = successCnt;
									callback(response);
									commonUtil.logPrint('[메모 삭제] 메모 삭제 완료, 메모 목록 불러오기 성공, ' + account);
								}
								//메모 삭제 완료, 메모 가져오기 실패.
								else {
									response.status = 321;
									response.data = null;
									response.tryCnt = tryCnt;
									response.successCnt = successCnt;
									callback(response);
									commonUtil.logPrint('[메모 삭제] 메모 삭제 완료, 메모 목록 불러오기 실패, ' + account);
								}
							});
						}
						//전달받은 메모 목록이 존재하면, 메모 삭제를 수행한다.
						else {
							for (var i = 0; i < memoIds.length; i++) {
								if (memoIds[i]) {
									dbUtil.findAndRemoveMemo(memoIds[i], account, function(isExist, isSuccess) {
										if (isExist && isSuccess) {
											successCnt++;
										}
										
										//성공, 실패 여부와 상관없이 시도 카운트를 하나 늘린다.
										tryCnt++;
										//시도 카운트가 전달받은 메모의 길이와 같으면, 모든 메모에 대한 삭제가 완료된 것으로 간주한다.
										if (tryCnt == memoIds.length) {
											dbUtil.getMemos(account, type, function(isSuccess, data) {
												//메모 삭제 완료, 메모 가져오기 성공.
												if (isSuccess) {
													response.status = 320;
													response.data = data;
													response.tryCnt = tryCnt;
													response.successCnt = successCnt;
													callback(response);
													commonUtil.logPrint('[메모 삭제] 메모 삭제 완료, 메모 목록 불러오기 성공, ' + account);
												}
												//메모 삭제 완료, 메모 가져오기 실패.
												else {
													response.status = 321;
													response.data = null;
													response.tryCnt = tryCnt;
													response.successCnt = successCnt;
													callback(response);
													commonUtil.logPrint('[메모 삭제] 메모 삭제 완료, 메모 목록 불러오기 실패, ' + account);
												}
											});
										}
									});
								}
								else {
									//성공, 실패 여부와 상관없이 시도 카운트를 하나 늘린다.
									tryCnt++;
									//시도 카운트가 전달받은 메모의 길이와 같으면, 모든 메모에 대한 삭제가 완료된 것으로 간주한다.
									if (tryCnt == memoIds.length) {
										dbUtil.getMemos(account, type, function(isSuccess, data) {
											//메모 삭제 완료, 메모 가져오기 성공.
											if (isSuccess) {
												response.status = 320;
												response.data = data;
												response.tryCnt = tryCnt;
												response.successCnt = successCnt;
												callback(response);
												commonUtil.logPrint('[메모 삭제] 메모 삭제 완료, 메모 목록 불러오기 성공, ' + account);
											}
											//메모 삭제 완료, 메모 가져오기 실패.
											else {
												response.status = 321;
												response.data = null;
												response.tryCnt = tryCnt;
												response.successCnt = successCnt;
												callback(response);
												commonUtil.logPrint('[메모 삭제] 메모 삭제 완료, 메모 목록 불러오기 실패, ' + account);
											}
										});
									}
								}
							}
						}
					}
					//사용자의 정보에 일치하는 인증키가 존재하지 않음. -> 인증키가 유효하지 않음.
					else {
						response.status = 103;
						response.data = null;
						response.tryCnt = 0;
						response.successCnt = 0;
						callback(response);
						commonUtil.logPrint('[메모 삭제] 올바르지 않은 인증키, ' + account);
					}
				}
				//사용자의 정보가 존재하지 않음. -> 인증키가 유효하지 않음.
				else {
					response.status = 103;
					response.data = null;
					response.tryCnt = 0;
					response.successCnt = 0;
					callback(response);
					commonUtil.logPrint('[메모 삭제] 로그인 정보가 존재하지 않음, ' + account);
				}
			}
			else {
				response.status = 103;
				response.data = null;
				response.tryCnt = 0;
				response.successCnt = 0;
				callback(response);
				commonUtil.logPrint('[메모 삭제] 전달받은 값이 올바르지 않음, ' + account);
			}
		});
		
		// 웹에서 메모 수정 시 발생하는 서버 이벤트
		socket.on('modifyWebMemo', function(req, callback) {
			console.log('modifyWebMemo 호출');
			
			var account = req.account;
			var authKey = req.auth;
			var type = WEB_USER_TYPE;
			var memoId = req.data._id;
			var memo = req.data.memo;
			var date = (new Date()).getTime();
			
			var response = {};
			
			var userData;
			
			if (account && authKey && type && memoId) {
				userData = mapUtil.getUserData(account);
				
				//사용자의 정보가 존재함.
				if (userData) {
					//사용자의 정보에 일치하는 인증키가 존재함.
					if (userData.isExistAuthKey(authKey)) {
						//서버에 id에 해당하는 메모가 존재하는지 확인한다.
						dbUtil.isExistMemo(memoId, account, function(isExist) {
							//서버에 id에 해당하는 메모가 존재하면, 메로를 수정한다.
							if (isExist) {
								dbUtil.modifyMemo(memoId, account, memo, date, function(isSuccess) {
									//메모 수정 성공.
									if (isSuccess) {
										//메모를 가져온다.
										dbUtil.getMemos(account, type, function(isSuccess, data) {
											//메모 수정 성공, 메모 가져오기 성공.
											if (isSuccess) {
												response.status = 330;
												response.data = data;
												callback(response);
												commonUtil.logPrint('[메모 수정] 메모 수정 성공, 메모 목록 불러오기 성공, ' + account);
											}
											//메모 수정 성공, 메모 가져오기 실패.
											else {
												response.status = 331;
												response.data = null;
												callback(response);
												commonUtil.logPrint('[메모 수정] 메모 수정 성공, 메모 목록 불러오기 실패, ' + account);
											}
										});
									}
									//메모 수정 실패.
									else {
										response.status = 332;
										response.data = null;
										callback(response);
										commonUtil.logPrint('[메모 수정] 메모 수정 실패, ' + account);
									}
								});
							}
							//서버에 id에 해당하는 메모가 존재하지 않으면, 메모를 기록한다.
							//-> 2013. 4. 15. 메모를 기록하지 않고, 응답코드를 수정 성공으로 하여 메모 목록을 보내주는 것으로 결정됨.
							else {
								//메모를 가져온다.
								dbUtil.getMemos(account, type, function(isSuccess, data) {
									//메모 수정 성공, 메모 가져오기 성공.
									if (isSuccess) {
										response.status = 330;
										response.data = data;
										callback(response);
										commonUtil.logPrint('[메모 수정] 메모 수정 성공, 메모 목록 불러오기 성공, ' + account);
									}
									//메모 수정 성공, 메모 가져오기 실패.
									else {
										response.status = 331;
										response.data = null;
										callback(response);
										commonUtil.logPrint('[메모 수정] 메모 수정 성공, 메모 목록 불러오기 실패, ' + account);
									}
								});
							}
						});
					}
					//사용자의 정보에 일치하는 인증키가 존재하지 않음. -> 인증키가 유효하지 않음.
					else {
						response.status = 103;
						response.data = null;
						callback(response);
						commonUtil.logPrint('[메모 수정] 올바르지 않은 인증키, ' + account);
					}
				}
				//사용자의 정보가 존재하지 않음. -> 인증키가 유효하지 않음.
				else {
					response.status = 103;
					response.data = null;
					callback(response);
					commonUtil.logPrint('[메모 수정] 로그인 정보가 존재하지 않음, ' + account);
				}
			}
			else {
				response.status = 103;
				response.data = null;
				callback(response);
				commonUtil.logPrint('[메모 수정] 전달받은 값이 올바르지 않음, ' + account);
			}
		});
		
		// 웹에서 로그아웃 시 발생하는 서버 이벤트
		socket.on('logout', function(req, callback) {
			var account = req.account;
			var authKey = req.auth;
			
			var response = {};
			
			var userData;
			
			if (account && authKey) {
				userData = mapUtil.getUserData(account);
				
				//사용자의 정보가 존재함.
				if (userData) {
					//사용자의 정보에 일치하는 인증키가 존재함.
					if (userData.isExistAuthKey(authKey)) {
						//로그아웃을 위하여 사용자 정보 삭제.
						userData.removeAuthKey(authKey);
						//갱신된 사용자 정보를 사용자 인증 저장소에 덮어씌움.
						mapUtil.putUserData(account, userData);
						
						response.status = 100;
						callback(response);
						commonUtil.logPrint('[로그아웃] 로그아웃 성공, ' + account);
					}
					//사용자의 정보에 일치하는 인증키가 존재하지 않음. -> 인증키가 유효하지 않음.
					else {
						response.status = 103;
						callback(response);
						commonUtil.logPrint('[로그아웃] 올바르지 않은 인증키, ' + account);
					}
				}
				//사용자의 정보가 존재하지 않음. -> 인증키가 유효하지 않음.
				else {
					response.status = 103;
					callback(response);
					commonUtil.logPrint('[로그아웃] 로그인 정보가 존재하지 않음, ' + account);
				}
			}
			else {
				response.status = 103;
				callback(response);
				commonUtil.logPrint('[로그아웃] 전달받은 값이 올바르지 않음, ' + account);
			}
		});
		
		
		socket.on('requestRemovedMemos', function(req) {
			var account = req.account;
			var authKey = req.auth;
			
			var response = {};
			
			var userData = mapUtil.getUserData(account);
			
			if (account && authKey && type) {
				//사용자의 정보가 존재함.
				if (userData) {
					//사용자의 정보에 일치하는 인증키가 존재함.
					if (userData.isExistAuthKey(authKey)) {
						//삭제된 메모를 가져온다.
						dbUtil.getRemovedMemos(account, function(isSuccess, data) {
							//메모 가져오기 성공.
							if (isSuccess) {
								response.status = 370;
								response.data = data;
								socket.emit('requestRemovedMemos', response);
								commonUtil.logPrint('[메모 요청] 메모 목록 불러오기 성공, ' + account);
							}
							//메모 가져오기 실패.
							else {
								response.status = 371;
								response.data = null;
								socket.emit('requestRemovedMemos', response);
								commonUtil.logPrint('[메모 요청] 메모 목록 불러오기 실패, ' + account);
							}
						});
					}
					//사용자의 정보에 일치하는 인증키가 존재하지 않음. -> 인증키가 유효하지 않음.
					else {
						response.status = 103;
						response.data = null;
						socket.emit('requestRemovedMemos', response);
						commonUtil.logPrint('[메모 요청] 올바르지 않은 인증키, ' + account);
					}
				}
				//사용자의 정보가 존재하지 않음. -> 인증키가 유효하지 않음.
				else {
					response.status = 103;
					response.data = null;
					socket.emit('requestRemovedMemos', response);
					commonUtil.logPrint('[메모 요청] 로그인 정보가 존재하지 않음, ' + account);
				}
			}
			else {
				response.status = 103;
				response.data = null;
				socket.emit('requestRemovedMemos', response);
				commonUtil.logPrint('[메모 요청] 전달받은 값이 올바르지 않음, ' + account);
			}
		});
		
		socket.on('restoreMemo', function(req) {
			var account = req.account;
			var authKey = req.auth;
			var type = WEB_USER_TYPE;
			var memoIds = req.data;
			
			var response = {};
			
			var userData;
			var successCnt = 0, tryCnt = 0;
			
			if (account && authKey && type && memoIds) {
				userData = mapUtil.getUserData(account)
				
				//사용자의 정보가 존재함.
				if (userData) {
					//사용자의 정보에 일치하는 인증키가 존재함.
					if (userData.isExistAuthKey(authKey)) {
						//전달받은 메모 목록이 존재하지 않으면, 서버에 저장된 메모 목록을 보낸다.
						if (memoIds.length == 0) {
							dbUtil.getMemos(account, type, function(isSuccess, data) {
								//메모 복구 완료, 메모 가져오기 성공.
								if (isSuccess) {
									response.status = 320;
									response.data = data;
									response.tryCnt = tryCnt;
									response.successCnt = successCnt;
									socket.emit('restoreMemo', response);
									commonUtil.logPrint('[메모 복구] 메모 복구 완료, 메모 목록 불러오기 성공, ' + account);
								}
								//메모 복구 완료, 메모 가져오기 실패.
								else {
									response.status = 321;
									response.data = null;
									response.tryCnt = tryCnt;
									response.successCnt = successCnt;
									socket.emit('restoreMemo', response);
									commonUtil.logPrint('[메모 복구] 메모 복구 완료, 메모 목록 불러오기 실패, ' + account);
								}
							});
						}
						//전달받은 메모 목록이 존재하면, 메모 복구를 수행한다.
						else {
							for (var i = 0; i < memoIds.length; i++) {
								if (memoIds[i]) {
									dbUtil.restoreMemo(memoIds[i], account, function(isExist, isSuccess) {
										if (isExist && isSuccess) {
											successCnt++;
										}
										
										//성공, 실패 여부와 상관없이 시도 카운트를 하나 늘린다.
										tryCnt++;
										//시도 카운트가 전달받은 메모의 길이와 같으면, 모든 메모에 대한 삭제가 완료된 것으로 간주한다.
										if (tryCnt == memoIds.length) {
											dbUtil.getMemos(account, type, function(isSuccess, data) {
												//메모 삭제 완료, 메모 가져오기 성공.
												if (isSuccess) {
													response.status = 320;
													response.data = data;
													response.tryCnt = tryCnt;
													response.successCnt = successCnt;
													socket.emit('restoreMemo', response);
													commonUtil.logPrint('[메모 삭제] 메모 삭제 완료, 메모 목록 불러오기 성공, ' + account);
												}
												//메모 삭제 완료, 메모 가져오기 실패.
												else {
													response.status = 321;
													response.data = null;
													response.tryCnt = tryCnt;
													response.successCnt = successCnt;
													socket.emit('restoreMemo', response);
													commonUtil.logPrint('[메모 삭제] 메모 삭제 완료, 메모 목록 불러오기 실패, ' + account);
												}
											});
										}
									});
								}
								else {
									//성공, 실패 여부와 상관없이 시도 카운트를 하나 늘린다.
									tryCnt++;
									//시도 카운트가 전달받은 메모의 길이와 같으면, 모든 메모에 대한 복구가 완료된 것으로 간주한다.
									if (tryCnt == memoIds.length) {
										dbUtil.getMemos(account, type, function(isSuccess, data) {
											//메모 삭제 완료, 메모 가져오기 성공.
											if (isSuccess) {
												response.status = 320;
												response.data = data;
												response.tryCnt = tryCnt;
												response.successCnt = successCnt;
												socket.emit('restoreMemo', response);
												commonUtil.logPrint('[메모 복구] 메모 복구 완료, 메모 목록 불러오기 성공, ' + account);
											}
											//메모 삭제 완료, 메모 가져오기 실패.
											else {
												response.status = 321;
												response.data = null;
												response.tryCnt = tryCnt;
												response.successCnt = successCnt;
												socket.emit('restoreMemo', response);
												commonUtil.logPrint('[메모 복구] 메모 복구 완료, 메모 목록 불러오기 실패, ' + account);
											}
										});
									}
								}
							}
						}
					}
					//사용자의 정보에 일치하는 인증키가 존재하지 않음. -> 인증키가 유효하지 않음.
					else {
						response.status = 103;
						response.data = null;
						response.tryCnt = 0;
						response.successCnt = 0;
						socket.emit('restoreMemo', response);
						commonUtil.logPrint('[메모 복구] 올바르지 않은 인증키, ' + account);
					}
				}
				//사용자의 정보가 존재하지 않음. -> 인증키가 유효하지 않음.
				else {
					response.status = 103;
					response.data = null;
					response.tryCnt = 0;
					response.successCnt = 0;
					socket.emit('restoreMemo', response);
					commonUtil.logPrint('[메모 복구] 로그인 정보가 존재하지 않음, ' + account);
				}
			}
			else {
				response.status = 103;
				response.data = null;
				response.tryCnt = 0;
				response.successCnt = 0;
				socket.emit('restoreMemo', response);
				commonUtil.logPrint('[메모 복구] 전달받은 값이 올바르지 않음, ' + account);
			}
		});
	});
};