﻿//사용자 정보 저장소.
HashMap = function() {
	this.map = new Array();
};

//사용자 정보 저장소 Prototype. 
HashMap.prototype = {
	put : function(key, value) {
		this.map[key] = value;
	},
	
	get : function(key) {
		return this.map[key];
	},
	
	remove : function(key) {
		this.map[key] = null;
	},
	
	clear : function() {
		this.map = new Array();
	}
};

//사용자 정보 자료형.
UserData = function() {
	this.authKeyList = new Array();
};

UserData.prototype = {
	//UserData에 새로운 AuthKey를 추가한다.
	addAuthKey : function(authKey) {
		if (authKey && !this.isExistAuthKey(authKey)) {
			this.authKeyList.push(authKey);
		}
	},
	
	//UserData에 있는 AuthKey를 제거한다.
	removeAuthKey : function(authKey) {
		if (this.authKeyList.length > 0 && authKey) {
			for (var i = 0; i < this.authKeyList.length; i++) {
				if (this.authKeyList[i] === authKey) {
					this.authKeyList = this.authKeyList.slice(0, i).concat(this.authKeyList.slice(i + 1, this.authKeyList.length));
					break;
				}
			}
		}
	},
	
	//UserData에 AuthKey가 있는지 확인한다.
	isExistAuthKey : function(authKey) {
		if (authKey) {
			for (var i = 0; i < this.authKeyList.length; i++) {
				if (this.authKeyList[i] === authKey) {
					return true;
				}
			}
		}
		
		return false;
	},
};

var userDataHashMap = new HashMap();

//UserDataHashMap에 새로운 UserData를 추가한다.
exports.putUserData = function(key, value) {
	userDataHashMap.put(key, value);
};

//UserDataHashMap에 있는 UserData를 가져온다.
exports.getUserData = function(key) {
	return userDataHashMap.get(key);
};

//UserDataHashMap에 있는 UserData를 제거한다.
exports.remmoveUserData = function(key) {
	userDataHashMap.remove(key);
};