﻿var mongodb = require('mongolian')
	, server = new mongodb
	, db = server.db('fastmemo')
	, users = db.collection('users')
	, memos = db.collection('memos')
	, trashs = db.collection('trashs')
	, commonUtil = require('./commonUtil');
	
//존재하는 사용자 아이디인지 확인한다.
exports.isExistUserId = function(account, callback) {
	users.findOne({ 'account': account }, function(err, result) {
		if (err) {
			commonUtil.logPrint('[ERROR] isExistUserId : ' + err);
			callback(false);
		}
		else {
			//사용자 아이디가 존재한다.
			if (result) {
				callback(true);
			}
			//사용자 아이디가 존재하지 않는다.
			else {
				callback(false);
			}
		}
	});
};

//새로운 회원을 등록한다.
exports.registryNewUser = function(account, pwd, callback) {
	users.findOne({ 'account': account }, function(err, result) {
		if (err) {
			commonUtil.logPrint('[ERROR] join : ' + err);
			callback(false);
		}
		else {
			//이미 가입된 사용자 아이디.
			if (result) {
				callback(false);
			}
			else {
				users.insert({ 'account': account, 'pwd': pwd }, function(err, result) {
					if (err) {
						commonUtil.logPrint('[ERROR] join : ' + err);
						callback(false);
					}
					else {
						//회원 등록 성공.
						if (result) {
							callback(true);
						}
						//회원 등록 실패.
						else {
							callback(false);
						}
					}
				});
			}
		}
	});
};

//올바른 사용자인지 확인한다.
exports.isValidUser = function(account, pwd, callback) {
	users.findOne({ 'account': account, 'pwd': pwd }, function(err, result) {
		if (err) {
			commonUtil.logPrint('[ERROR] isValidUser : ' + err);
			callback(false);
		}
		else {
			if (result) {
				callback(true);
			}
			else {
				callback(false);
			}
		}
	});
};

//메모가 존재하는지 확인한다.
exports.isExistMemo = function(id, account, callback) {
	if (id) {
		memos.findOne({ '_id': new mongodb.ObjectId(id), 'account': account }, function(err, result) {
			if (err) {
				commonUtil.logPrint('[ERROR] isExistMemo : ' + err);
				callback(false);
			}
			else {
				if (result) {
					callback(true);
				}
				else {
					callback(false);
				}
			}
		});	
	}
	else {
		callback(false);
	}
};

exports.getMemo = function(id, account, callback) {
	memos.findOne({ '_id': new mongodb.ObjectId(id), 'account': account }, function(err, result) {
		if (err) {
			commonUtil.logPrint('[ERROR] getMemo : ' + err);
			callback(null);
		}
		else {
			if (result) {
				callback(result);
			}
			else {
				callback(null);
			}
		}
	});
};

//메모를 가져온다.
exports.getMemos = function(account, type, callback) {
	switch (type) {
	case 0:	//Lite 사용자
		memos.find({ 'account': account }, { 'account': 0 }).sort({ 'date': -1 }).limit(10).toArray(function(err, array) {
			if (err) {
				commonUtil.logPrint('[ERROR] getMemos : ' + err);
				callback(false, null);
			}
			else {
				if (array) {
					for (var i = 0; i < array.length; i++) {
						array[i]._id = array[i]._id.toString();
					}
					callback(true, array);
				}
				else {
					callback(false, null);
				}
			}
		});
		break;
	case 1:	//Pro 사용자
		memos.find({ 'account': account }, { 'account': 0 }).sort({ 'date': -1 }).toArray(function(err, array) {
			if (err) {
				commonUtil.logPrint('[ERROR] getMemos : ' + err);
				callback(false, null);
			}
			else {
				if (array) {
					for (var i = 0; i < array.length; i++) {
						array[i]._id = array[i]._id.toString();
					}
					callback(true, array);
				}
				else {
					callback(false, null);
				}
			}
		});
		break;
	default:
		callback(false, null);
	}
};

// 앱에서 메모를 작성하기 위한 함수,
exports.writeMemo = function(id, account, memo, date, callback) {
	memos.insert({ '_id': new mongodb.ObjectId(id), 'account': account, 'memo': memo, 'date': date }, function(err, result) {
		if (err) {
			commonUtil.logPrint('[ERROR] write To AppMemo : ' + err);
			callback(false);
		}
		else {
			if (result) {
				callback(true);
			}
			else {
				callback(false);
			}
		}
	});
};

// 웹에서 메모를 작성하기 위한 함수,
exports.writeWebMemo = function(account, memo, date, callback) {
	console.log('writeWebMemo 호출');

	memos.insert({'account': account, 'memo': memo, 'date': date}, function(err, result) {
		if(err) {
			commonUtil.logPrint('[ERROR] write To WebMemo : ' + err);
			callback(false);
		}
		else {
			if(result) {
				callback(true);
			}
			else {
				callback(false);
			}
		}
	});
}

//메모를 수정한다.
exports.modifyMemo = function(id, account, memo, date, callback) {
	memos.update({ '_id': new mongodb.ObjectId(id), 'account': account }, { $set: { 'memo': memo, 'date': date }}, function(err, result) {
		if (err) {
			commonUtil.logPrint('[ERROR] modifyMemo : ' + err);
			callback(false);
		}
		else {
			if (result) {
				callback(true);
			}
			else {
				callback(false);
			}
		}
	});
};

//메모를 삭제한다. 삭제한 메모는 쓰레기통에 반영구적으로 보존한다.
//쓰레기통으로 옮기지 않고 서버에서 먼저 삭제하면, 삭제 후 쓰레기통으로 옮길 때 에러 발생 시 복구할 수 없다.
//따라서 쓰레기통으로 옮기고 서버에서 삭제하여, 어떠한 경우에도 메모를 복원할 수 있도록 한다.
exports.findAndRemoveMemo = function(id, account, callback) {
	var objectId = new mongodb.ObjectId(id);
	
	memos.findOne({ '_id': objectId, 'account': account }, function(err, result) {
		if (err) {
			commonUtil.logPrint('[ERROR] findAndRemoveMemo : ' + err);
			callback(false, false);
		}
		else {
			//서버에 id에 해당하는 메모가 존재할 경우, 서버에 존재하는 메모를 삭제한다.
			if (result) {
				var trash = result;
			
				//삭제할 메모가 쓰레기통에 있는지 확인한다.
				trashs.findOne({ '_id': trash._id, 'account': trash.account}, function(err, result) {
					if (err) {
						commonUtil.logPrint('[ERROR] findAndRemoveMemo : ' + err);
						callback(true, false);
					}
					else {
						//삭제할 메모가 쓰레기통에 있으면, 이미 삭제된 메모이다.
						if (result) {
							//하지만 현재 서버에 삭제된 메모가 남아있으므로, 서버에 저장된 메모를 삭제한다.
							memos.remove({ '_id': result._id, 'account': result.account }, function(err, result) {
								if (err) {
									commonUtil.logPrint('[ERROR] findAndRemoveMemo : ' + err);
									callback(true, false);
								}
								else {
									//메모 삭제 완료.
									if (result) {
										callback(true, true);
									}
									else {
										callback(true, false);
									}
								}
							});
						}
						//삭제할 메모가 쓰레기통에 없으면, 쓰레기통에 메모를 넣는다.
						else {
							trashs.insert({ '_id': trash._id, 'account': trash.account, 'memo': trash.memo, 'date': trash.date }, function(err, result) {
								if (err) {
									commonUtil.logPrint('[ERROR] findAndRemoveMemo : ' + err);
									callback(true, false);
								}
								else {
									//삭제할 메모가 쓰레기통으로 옮겨졌으면, 서버에서 메모를 삭제한다.
									if (result) {
										memos.remove({ '_id': trash._id, 'account': trash.account }, function(err, result) {
											if (err) {
												commonUtil.logPrint('[ERROR] findAndRemoveMemo : ' + err);
												callback(true, false);
											}
											else {
												//메모 삭제 완료.
												if (result) {
													callback(true, true);
												}
												else {
													callback(true, false);
												}
											}
										});
									}
									else {
										callback(true, false);
									}
								}
							});
						}
					}
				});
			}
			//서버에 id에 해당하는 메모가 없을 때는, 메모 삭제에 성공한 것으로 간주한다.
			else {
				callback(true, true);
			}
		}
	});
};

//서버와 동기화 작업을 수행한다.
exports.findAndSyncMemo = function(id, account, memo, date, callback) {
	var objectId = new mongodb.ObjectId(id);
	
	//서버에 id에 해당하는 메모가 존재하는지 확인한다.
	memos.findOne({ '_id': objectId, 'account': account }, function(err, result) {
		if (err) {
			commonUtil.logPrint('[ERROR] findAndSyncMemo : ' + err);
			callback(false, false);
		}
		else {
			//서버에 id에 해당하는 메모가 존재할 때,
			if (result) {
				//쓰레기통에 id에 해당하는 메모가 존재할 때,
				var server = result;
				
				trashs.findOne({ '_id': objectId, 'account': account }, function(err, result) {
					if (err) {
						commomUtil.logPrint('[ERROR] findAndSyncMemo : ' + err);
						callback(true, false);
					}
					else {
						//삭제 기록이 있으나, 서버에서 제대로 삭제가 되지 않은 메모이다. 따라서 서버에서 메모를 삭제한다.
						if (result) {
							memos.remove({ '_id': objectId, 'account': account }, function(err, result) {
								if (err) {
									commomUtil.logPrint('[ERROR] findAndSyncMemo : ' + err);
									callback(true, false);
								}
								else {
									if (result) {
										callback(true, true);
									}
									else {
										callback(true, false);
									}
								}
							});
						}
						//삭제 기록이 없으면, 서버에 있는 메모와 전달받은 메모 중 최근 메모를 판별하여 갱신힌다.
						else {
							//전달받은 메모의 날짜가 서버 메모의 날짜보다 최근이면, 서버의 메모를 갱신한다.
							if (date > server.date) {
								memos.update({ '_id': objectId, 'account': account }, { $set: { 'memo': memo, 'date': date }}, function(err, result) {
									if (err) {
										commonUtil.logPrint('[ERROR] findAndSyncMemo : ' + err);
										callback(true, false);
									}
									else {
										if (result) {
											callback(true, true);
										}
										else {
											callback(true, false);
										}
									}
								});
							}
							//서버 메모의 날짜가 전달받은 메모의 날짜보다 최근이면, 아무런 작업도 하지 않는다. 이 경우 동기화가 성공한 것으로 본다.
							else {
								callback(true, true);
							}
						}
					}
				});
			}
			//서버에 id에 해당하는 메모가 존재하지 않을 때,
			else {
				//쓰레기통에 id에 해당하는 메모가 존재하는지 확인한다.
				trashs.findOne({ '_id': objectId, 'account': account }, function(err, result) {
					if (err) {
						commonUtil.logPrint('[ERROR] findAndSyncMemo : ' + err);
						callback(false, false);
					}
					else {
						//쓰레기통에 id에 해당하는 메모가 존재하면, 삭제된 메모이므로 아무런 작업도 하지 않는다. 이 경우 동기화가 성공한 것으로 본다.
						if (result) {
							callback(true, true);
						}
						//쓰레기통에 id에 해당하는 메모가 존재하지 않으면, 서버에 메모를 기록한다.
						else {
							memos.insert({ '_id': objectId, 'account': account, 'memo': memo, 'date': date }, function(err, result) {
								if (err) {
									commonUtil.logPrint('[ERROR] findAndSyncMemo : ' + err);
									callback(true, false);
								}
								else {
									if (result) {
										callback(true, true);
									}
									else {
										callback(true, false);
									}
								}
							});
						}
					}
				});
			}
		}
	});
};

//메모를 가져온다.
exports.getRemovedMemos = function(account, callback) {
	trashs.find({ 'account': account }, { 'account': 0 }).sort({ 'date': -1 }).toArray(function(err, array) {
		if (err) {
			commonUtil.logPrint('[ERROR] getRemovedMemos : ' + err);
			callback(false, null);
		}
		else {
			if (array) {
				for (var i = 0; i < array.length; i++) {
					array[i]._id = array[i]._id.toString();
				}
				callback(true, array);
			}
			else {
				callback(false, null);
			}
		}
	});
};

//쓰레기통의 메모를 복원한다.
exports.restoreMemo = function(id, account, callback) {
	var objectId = new mongodb.ObjectId(id);
	
	trashs.findOne({ '_id': objectId, 'account': account }, function(err, result) {
		if (err) {
			commonUtil.logPrint('[ERROR] restoreMemo : ' + err);
			callback(false, false);
		}
		else {
			if (result) {
				var restore = result;
				
				memos.insert({ '_id': restore._id, 'account': restore.account, 'memo': restore.memo, 'date': restore.date }, function(err, result) {
					if (err) {
						commonUtil.logPrint('[ERROR] restoreMemo : ' + err);
						callback(true, false);
					}
					else {
						//이 시점에서 복구는 성공. 하지만 복구한 이후에 쓰레기통에서 해당 메모를 삭제해야지만 완벽하게 복구된다.
						//여기서 쓰레기통에 있는 메모를 삭제하지 못하면 결과적으로 서버는 해당 메모를 삭제된 메모로 판단하게 된다.
						//따라서 결국에 아래 작업이 실패하면, 메모가 복구되더라도 다시 삭제된다.
						if (result) {
							//쓰레기통에서 복구한 메모를 삭제한다.
							trashs.remove({ '_id': restore._id, 'account': restore.account }, function(err, result) {
								if (err) {
									commonUtil.logPrint('[ERROR] restoreMemo : ' + err);
									callback(true, false);
								}
								else {
									//삭제된 메모의 복구가 완료되었고, 쓰레기통에서 해당 메모를 성공적으로 삭제하였다. -> 복구 완료.
									if (result) {
										callback(true, true);
									}
									else {
										callback(true, false);
									}
								}
							});
						}
						else {
							callback(true, false);
						}
					}
				});
			}
			else {
				callback(false, false);
			}
		}
	});
};