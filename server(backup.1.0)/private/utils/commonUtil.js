﻿var developeMode = true;

//Console에 Log Message와 Timestamp를 출력한다.
exports.logPrint = function(msg) {
	if (developeMode)
		console.log(msg + ' [' + getTimestamp() + ']');
};

//현재 Timestamp를 가져온다.
getTimestamp = function() {
	var date = new Date();
	
	return time = leadingZero(date.getFullYear(),  4) + '/' +
				  leadingZero(date.getMonth() + 1, 2) + '/' +
				  leadingZero(date.getDate(),      2) + ' ' +
				  leadingZero(date.getHours(),     2) + ':' +
				  leadingZero(date.getMinutes(),   2) + ':' +
				  leadingZero(date.getSeconds(),   2);
};

//Timestamp Formatter.
leadingZero = function(n, digits) {
	var zero = '';
	n = n.toString();
	
	if (n.length < digits) {
		for (var i = 0; i < digits - n.length; i++) {
			zero += 0;
		}
	}
	
	return zero + n;
};