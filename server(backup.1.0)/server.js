
/**
 * Module dependencies.
 */

var express = require('express')
  , http = require('http')
  , path = require('path')
  , app_users = require('./routes/app_users')
  , app_memos = require('./routes/app_memos')
  , web_routes = require('./routes/web_routes')
  

var app = express();

app.configure(function(){
  app.set('port', process.env.PORT || 4000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.cookieParser());
  app.use(express.session({secret:'sessionkey'}));
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function(){
  app.use(express.errorHandler());
});

//web
app.get('/', web_routes.index);
app.get('/signup', web_routes.signup);
app.get('/process', web_routes.process_get);
app.get('/memo', web_routes.memo_get);

app.post('/process', web_routes.process_post);
app.post('/memo', web_routes.memo_post);

//application
app.post('/api/login',	app_users.login);
app.post('/api/logout',	app_users.logout);
app.post('/api/join',	app_users.join);

app.post('/api/memo/sync',		app_memos.syncMemo);
app.post('/api/memo/request',	app_memos.requestMemos);
app.post('/api/memo/write',		app_memos.writeMemo);
app.post('/api/memo/remove',	app_memos.removeMemos);
app.post('/api/memo/modify',	app_memos.modifyMemo);

var server = http.createServer(app);
require('./private/utils/socketUtil')(server);

server.listen(app.get('port'), function() {
	console.log("Express server listening on port " + app.get('port'));
});
