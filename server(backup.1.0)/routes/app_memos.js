﻿/*
application - memo control methods
*/

var dbUtil = require('../private/utils/dbUtil')
	, mapUtil = require('../private/utils/mapUtil')
	, commonUtil = require('../private/utils/commonUtil');

exports.syncMemo = function(req, res) {
	var account = req.body.account;
	var authKey = req.body.auth;
	var type = req.body.type;
	var memos = req.body.data;
	
	var response = {};
		
	var userData;
	var successCnt = 0, tryCnt = 0;
	
	if (account && authKey && type && memos) {
		userData = mapUtil.getUserData(account);
		
		//사용자의 정보가 존재함.
		if (userData) {
			//사용자의 정보에 일치하는 인증키가 존재함.
			if (userData.isExistAuthKey(authKey)) {
				//전달받은 메모 목록이 없을 때, 서버에 저장된 메모 목록을 보내준다.
				if (memos.length == 0) {
					dbUtil.getMemos(account, type, function(isSuccess, data) {
						//메모 동기화 완료, 메모 가져오기 성공.
						if (isSuccess) {
							response.status = 300;
							response.data = data;
							response.tryCnt = tryCnt;
							response.successCnt = successCnt;
							res.json(response);
							commonUtil.logPrint('[동기화] 동기화 완료, 메모 목록 불러오기 성공, ' + account);
						}
						//메모 동기화 완료, 메모 가져오기 실패.
						else {
							response.status = 301;
							response.data = null;
							response.tryCnt = tryCnt;
							response.successCnt = successCnt;
							res.json(response);
							commonUtil.logPrint('[동기화] 동기화 완료, 메모 목록 불러오기 실패, ' + account);
						}
					});
				}
				//전달받은 메모 목록이 있을 때, 서버에 저장된 메모 목록과 동기화 작업을 수행한다.
				else {
					for (var i = 0; i < memos.length; i++) {
						if (memos[i]._id && memos[i].memo && memos[i].date) {
							//서버와 메모 동기화 작업을 수행한다.							
							dbUtil.findAndSyncMemo(memos[i]._id, account, memos[i].memo, memos[i].date, function(isExist, isSuccess) {
								//서버에 _id에 해당하는 메모가 존재하고, 메모가 최신으로 수정되었을 때, 성공 카운트를 하나 늘린다.
								if (isExist && isSuccess) {
									successCnt++;
								}
								
								//성공, 실패 여부와 상관없이 시도 카운트를 하나 늘린다.
								tryCnt++;
								//시도 카운트가 전달받은 메모의 길이와 같으면, 모든 메모에 대한 동기화가 완료된 것으로 간주한다.
								if (tryCnt == memos.length) {
									dbUtil.getMemos(account, type, function(isSuccess, data) {
										//메모 동기화 완료, 메모 가져오기 성공.
										if (isSuccess) {
											response.status = 300;
											response.data = data;
											response.tryCnt = tryCnt;
											response.successCnt = successCnt;
											res.json(response);
											commonUtil.logPrint('[동기화] 동기화 완료, 메모 목록 불러오기 성공, ' + account);
										}
										//메모 동기화 완료, 메모 가져오기 실패.
										else {
											response.status = 301;
											response.data = null;
											response.tryCnt = tryCnt;
											response.successCnt = successCnt;
											res.json(response);
											commonUtil.logPrint('[동기화] 동기화 완료, 메모 목록 불러오기 실패, ' + account);
										}
									});
								}
							});
						}
						else {
							//성공, 실패 여부와 상관없이 시도 카운트를 하나 늘린다.
							tryCnt++;
							//시도 카운트가 전달받은 메모의 길이와 같으면, 모든 메모에 대한 동기화가 완료된 것으로 간주한다.
							if (tryCnt == memos.length) {
								dbUtil.getMemos(account, type, function(isSuccess, data) {
									//메모 동기화 완료, 메모 가져오기 성공.
									if (isSuccess) {
										response.status = 300;
										response.data = data;
										response.tryCnt = tryCnt;
										response.successCnt = successCnt;
										res.json(response);
										commonUtil.logPrint('[동기화] 동기화 완료, 메모 목록 불러오기 성공, ' + account);
									}
									//메모 동기화 완료, 메모 가져오기 실패.
									else {
										response.status = 301;
										response.data = null;
										response.tryCnt = tryCnt;
										response.successCnt = successCnt;
										res.json(response);
										commonUtil.logPrint('[동기화] 동기화 완료, 메모 목록 불러오기 실패, ' + account);
									}
								});
							}
						}
					}
				}
			}
			//사용자의 정보에 일치하는 인증키가 존재하지 않음. -> 인증키가 유효하지 않음.
			else {
				response.status = 103;
				response.data = null;
				response.tryCnt = 0;
				response.successCnt = 0;
				res.json(response);
				commonUtil.logPrint('[동기화] 올바르지 않은 인증키, ' + account);
			}
		}
		//사용자의 정보가 존재하지 않음. -> 인증키가 유효하지 않음.
		else {
			response.status = 103;
			response.data = null;
			response.tryCnt = 0;
			response.successCnt = 0;
			res.json(response);
			commonUtil.logPrint('[동기화] 로그인 정보가 존재하지 않음, ' + account);
		}
	}
	else {
		response.status = 103;
		response.data = null;
		response.tryCnt = 0;
		response.successCnt = 0;
		res.json(response);
		commonUtil.logPrint('[동기화] 전달받은 값이 올바르지 않음, ' + account);
	}
};

exports.writeMemo = function(req, res) {
	var account = req.body.account;
	var authKey = req.body.auth;
	var type = req.body.type;
	var memoId = req.body.data._id;
	var memo = req.body.data.memo;
	//var date = req.body.data.date;
	var date = (new Date()).getTime();
	
	var response = {};
	
	var userData;
	
	if (account && authKey && type && memoId && memo && date) {
		userData =  mapUtil.getUserData(account);
		
		//사용자의 정보가 존재함.
		if (userData) {
			//사용자의 정보에 일치하는 인증키가 존재함.
			if (userData.isExistAuthKey(authKey)) {
				//새로운 메모를 쓴다.
				dbUtil.writeMemo(memoId, account, memo, date, function(isSuccess) {
					//메모 쓰기 성공.
					if (isSuccess) {
						//메모 목록을 가져온다.
						dbUtil.getMemos(account, type, function(isSuccess, data) {
							//메모 쓰기 성공, 메모 가져오기 성공.
							if (isSuccess) {
								response.status = 310;
								response.data = data;
								res.json(response);
								commonUtil.logPrint('[메모 작성] 메모 작성 성공, 메모 목록 불러오기 성공, ' + account);
							}
							//메모 쓰기 성공, 메모 가져오기 실패.
							else {
								response.status = 311;
								response.data = null;
								res.json(response);
								commonUtil.logPrint('[메모 작성] 메모 작성 성공, 메모 목록 불러오기 실패, ' + account);
							}
						});
					}
					//메모 쓰기 실패.
					else {
						response.status = 312;
						response.data = null;
						res.json(response);
						commonUtil.logPrint('[메모 작성] 메모 작성 실패, ' + account);
					}
				});
			}
			//사용자의 정보에 일치하는 인증키가 존재하지 않음. -> 인증키가 유효하지 않음.
			else {
				response.status = 103;
				response.data = null;
				res.json(response);
				commonUtil.logPrint('[메모 작성] 올바르지 않은 인증키, ' + account);
			}
		}
		//사용자의 정보가 존재하지 않음. -> 인증키가 유효하지 않음.
		else {
			response.status = 103;
			response.data = null;
			res.json(response);
			commonUtil.logPrint('[메모 작성] 로그인 정보가 존재하지 않음, ' + account);
		}
	}
	else {
		response.status = 103;
		response.data = null;
		res.json(response);
		commonUtil.logPrint('[메모 작성] 전달받은 값이 올바르지 않음, ' + account);
	}
};

exports.removeMemos = function(req, res) {
	var account = req.body.account;
	var authKey = req.body.auth;
	var type = req.body.type;
	var memoIds = req.body.data;
	
	var response = {};
	
	var userData;
	var successCnt = 0, tryCnt = 0;
	
	if (account && authKey && type && memoIds) {
		userData = mapUtil.getUserData(account)
		
		//사용자의 정보가 존재함.
		if (userData) {
			//사용자의 정보에 일치하는 인증키가 존재함.
			if (userData.isExistAuthKey(authKey)) {
				//전달받은 메모 목록이 존재하지 않으면, 서버에 저장된 메모 목록을 보낸다.
				if (memoIds.length == 0) {
					dbUtil.getMemos(account, type, function(isSuccess, data) {
						//메모 삭제 완료, 메모 가져오기 성공.
						if (isSuccess) {
							response.status = 320;
							response.data = data;
							response.tryCnt = tryCnt;
							response.successCnt = successCnt;
							res.json(response);
							commonUtil.logPrint('[메모 삭제] 메모 삭제 완료, 메모 목록 불러오기 성공, ' + account);
						}
						//메모 삭제 완료, 메모 가져오기 실패.
						else {
							response.status = 321;
							response.data = null;
							response.tryCnt = tryCnt;
							response.successCnt = successCnt;
							res.json(response);
							commonUtil.logPrint('[메모 삭제] 메모 삭제 완료, 메모 목록 불러오기 실패, ' + account);
						}
					});
				}
				//전달받은 메모 목록이 존재하면, 메모 삭제를 수행한다.
				else {
					for (var i = 0; i < memoIds.length; i++) {
						if (memoIds[i]) {
							dbUtil.findAndRemoveMemo(memoIds[i], account, function(isExist, isSuccess) {
								if (isExist && isSuccess) {
									successCnt++;
								}
								
								//성공, 실패 여부와 상관없이 시도 카운트를 하나 늘린다.
								tryCnt++;
								//시도 카운트가 전달받은 메모의 길이와 같으면, 모든 메모에 대한 삭제가 완료된 것으로 간주한다.
								if (tryCnt == memoIds.length) {
									dbUtil.getMemos(account, type, function(isSuccess, data) {
										//메모 삭제 완료, 메모 가져오기 성공.
										if (isSuccess) {
											response.status = 320;
											response.data = data;
											response.tryCnt = tryCnt;
											response.successCnt = successCnt;
											res.json(response);
											commonUtil.logPrint('[메모 삭제] 메모 삭제 완료, 메모 목록 불러오기 성공, ' + account);
										}
										//메모 삭제 완료, 메모 가져오기 실패.
										else {
											response.status = 321;
											response.data = null;
											response.tryCnt = tryCnt;
											response.successCnt = successCnt;
											res.json(response);
											commonUtil.logPrint('[메모 삭제] 메모 삭제 완료, 메모 목록 불러오기 실패, ' + account);
										}
									});
								}
							});
						}
						else {
							//성공, 실패 여부와 상관없이 시도 카운트를 하나 늘린다.
							tryCnt++;
							//시도 카운트가 전달받은 메모의 길이와 같으면, 모든 메모에 대한 삭제가 완료된 것으로 간주한다.
							if (tryCnt == memoIds.length) {
								dbUtil.getMemos(account, type, function(isSuccess, data) {
									//메모 삭제 완료, 메모 가져오기 성공.
									if (isSuccess) {
										response.status = 320;
										response.data = data;
										response.tryCnt = tryCnt;
										response.successCnt = successCnt;
										res.json(response);
										commonUtil.logPrint('[메모 삭제] 메모 삭제 완료, 메모 목록 불러오기 성공, ' + account);
									}
									//메모 삭제 완료, 메모 가져오기 실패.
									else {
										response.status = 321;
										response.data = null;
										response.tryCnt = tryCnt;
										response.successCnt = successCnt;
										res.json(response);
										commonUtil.logPrint('[메모 삭제] 메모 삭제 완료, 메모 목록 불러오기 실패, ' + account);
									}
								});
							}
						}
					}
				}
			}
			//사용자의 정보에 일치하는 인증키가 존재하지 않음. -> 인증키가 유효하지 않음.
			else {
				response.status = 103;
				response.data = null;
				response.tryCnt = 0;
				response.successCnt = 0;
				res.json(response);
				commonUtil.logPrint('[메모 삭제] 올바르지 않은 인증키, ' + account);
			}
		}
		//사용자의 정보가 존재하지 않음. -> 인증키가 유효하지 않음.
		else {
			response.status = 103;
			response.data = null;
			response.tryCnt = 0;
			response.successCnt = 0;
			res.json(response);
			commonUtil.logPrint('[메모 삭제] 로그인 정보가 존재하지 않음, ' + account);
		}
	}
	else {
		response.status = 103;
		response.data = null;
		response.tryCnt = 0;
		response.successCnt = 0;
		res.json(response);
		commonUtil.logPrint('[메모 삭제] 전달받은 값이 올바르지 않음, ' + account);
	}
};

exports.modifyMemo = function(req, res) {
	var account = req.body.account;
	var authKey = req.body.auth;
	var type = req.body.type;
	var memoId = req.body.data._id;
	var memo = req.body.data.memo;
	//var date = req.body.data.date;
	var date = (new Date()).getTime();
	
	var response = {};
	
	var userData;
	
	if (account && authKey && type && memoId && memo && date) {
		userData = mapUtil.getUserData(account);
		
		//사용자의 정보가 존재함.
		if (userData) {
			//사용자의 정보에 일치하는 인증키가 존재함.
			if (userData.isExistAuthKey(authKey)) {
				//서버에 id에 해당하는 메모가 존재하는지 확인한다.
				dbUtil.isExistMemo(memoId, account, function(isExist) {
					//서버에 id에 해당하는 메모가 존재하면, 메로를 수정한다.
					if (isExist) {
						dbUtil.modifyMemo(memoId, account, memo, date, function(isSuccess) {
							//메모 수정 성공.
							if (isSuccess) {
								//메모를 가져온다.
								dbUtil.getMemos(account, type, function(isSuccess, data) {
									//메모 수정 성공, 메모 가져오기 성공.
									if (isSuccess) {
										response.status = 330;
										response.data = data;
										res.json(response);
										commonUtil.logPrint('[메모 수정] 메모 수정 성공, 메모 목록 불러오기 성공, ' + account);
									}
									//메모 수정 성공, 메모 가져오기 실패.
									else {
										response.status = 331;
										response.data = null;
										res.json(response);
										commonUtil.logPrint('[메모 수정] 메모 수정 성공, 메모 목록 불러오기 실패, ' + account);
									}
								});
							}
							//메모 수정 실패.
							else {
								response.status = 332;
								response.data = null;
								res.json(response);
								commonUtil.logPrint('[메모 수정] 메모 수정 실패, ' + account);
							}
						});
					}
					//서버에 id에 해당하는 메모가 존재하지 않으면, 메모를 기록한다.
					//-> 2013. 4. 15. 메모를 기록하지 않고, 응답코드를 수정 성공으로 하여 메모 목록을 보내주는 것으로 결정됨.
					else {
						//메모를 가져온다.
						dbUtil.getMemos(account, type, function(isSuccess, data) {
							//메모 수정 성공, 메모 가져오기 성공.
							if (isSuccess) {
								response.status = 330;
								response.data = data;
								res.json(response);
								commonUtil.logPrint('[메모 수정] 메모 수정 성공, 메모 목록 불러오기 성공, ' + account);
							}
							//메모 수정 성공, 메모 가져오기 실패.
							else {
								response.status = 331;
								response.data = null;
								res.json(response);
								commonUtil.logPrint('[메모 수정] 메모 수정 성공, 메모 목록 불러오기 실패, ' + account);
							}
						});
					}
				});
			}
			//사용자의 정보에 일치하는 인증키가 존재하지 않음. -> 인증키가 유효하지 않음.
			else {
				response.status = 103;
				response.data = null;
				res.json(response);
				commonUtil.logPrint('[메모 수정] 올바르지 않은 인증키, ' + account);
			}
		}
		//사용자의 정보가 존재하지 않음. -> 인증키가 유효하지 않음.
		else {
			response.status = 103;
			response.data = null;
			res.json(response);
			commonUtil.logPrint('[메모 수정] 로그인 정보가 존재하지 않음, ' + account);
		}
	}
	else {
		response.status = 103;
		response.data = null;
		res.json(response);
		commonUtil.logPrint('[메모 수정] 전달받은 값이 올바르지 않음, ' + account);
	}
};

exports.requestMemos = function(req, res) {
	var account = req.body.account;
	var authKey = req.body.auth;
	var type = req.body.type;
	
	var response = {};
	
	var userData = mapUtil.getUserData(account);
	
	if (account && authKey && type) {
		//사용자의 정보가 존재함.
		if (userData) {
			//사용자의 정보에 일치하는 인증키가 존재함.
			if (userData.isExistAuthKey(authKey)) {
				//메모를 가져온다.
				dbUtil.getMemos(account, type, function(isSuccess, data) {
					//메모 가져오기 성공.
					if (isSuccess) {
						response.status = 340;
						response.data = data;
						res.json(response);
						commonUtil.logPrint('[메모 요청] 메모 목록 불러오기 성공, ' + account);
					}
					//메모 가져오기 실패.
					else {
						response.status = 341;
						response.data = null;
						res.json(response);
						commonUtil.logPrint('[메모 요청] 메모 목록 불러오기 실패, ' + account);
					}
				});
			}
			//사용자의 정보에 일치하는 인증키가 존재하지 않음. -> 인증키가 유효하지 않음.
			else {
				response.status = 103;
				response.data = null;
				res.json(response);
				commonUtil.logPrint('[메모 요청] 올바르지 않은 인증키, ' + account);
			}
		}
		//사용자의 정보가 존재하지 않음. -> 인증키가 유효하지 않음.
		else {
			response.status = 103;
			response.data = null;
			res.json(response);
			commonUtil.logPrint('[메모 요청] 로그인 정보가 존재하지 않음, ' + account);
		}
	}
	else {
		response.status = 103;
		response.data = null;
		res.json(response);
		commonUtil.logPrint('[메모 요청] 전달받은 값이 올바르지 않음, ' + account);
	}
};