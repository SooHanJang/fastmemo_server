﻿/*
application - user control methods.
*/

var crypto = require('crypto')
	, dbUtil = require('../private/utils/dbUtil')
	, mapUtil = require('../private/utils/mapUtil')
	, commonUtil = require('../private/utils/commonUtil');

exports.login = function(req, res) {
	var account = req.body.account;
	var pwd = req.body.pwd;
	
	var response = {};
	
	if (account && pwd) {
		dbUtil.isExistUserId(account, function(isExist) {
			if (isExist) {
				//사용자 아이디와 비밀번호가 유효한지 확인한다.
				dbUtil.isValidUser(account, pwd, function(isValid) {
					//유효한 사용자 아이디와 패스워드.
					if (isValid) {
						var userData = mapUtil.getUserData(account);
						
						//등록된 로그인 정보가 없을 때, 로그인 정보를 생성한다.
						if (userData == null) {
							userData = new UserData();
						}
						
						//account, timestamp값 이용하여 인증키 생성
						var sha256 = crypto.createHash('sha256');
						sha256.update(account + new Date().getTime().toString());
						var authKey = sha256.digest('hex');
						
						//생성한 authKey 저장한다.
						userData.addAuthKey(authKey);
							
						mapUtil.putUserData(account, userData);
						
						response.status = 100;
						response.data = authKey;
						res.json(response);
						commonUtil.logPrint('[로그인] 로그인 성공, ' + account);
					}
					//올바르지 않은 패스워드.
					else {
						response.status = 102;
						response.data = null;
						res.json(response);
						commonUtil.logPrint('[로그인] 올바르지 않은 비밀번호, ' + account);
					}
				});
			}
			//존재하지 않는 사용자 아이디.
			else {
				response.status = 101;
				response.data = null;
				res.json(response);
				commonUtil.logPrint('[로그인] 올바르지 않은 계정, ' + account);
			}
		});
	}
	else {
		response.status = 101;
		response.data = null;
		res.json(response);
		commonUtil.logPrint('[로그인] 전달받은 값이 올바르지 않음, ' + account);
	}
};

exports.logout = function(req, res) {
	var account = req.body.account;
	var authKey = req.body.auth;
	
	var response = {};
	
	var userData;
	
	if (account && authKey) {
		userData = mapUtil.getUserData(account);
		
		//사용자의 정보가 존재함.
		if (userData) {
			//사용자의 정보에 일치하는 인증키가 존재함.
			if (userData.isExistAuthKey(authKey)) {
				//로그아웃을 위하여 사용자 정보 삭제.
				userData.removeAuthKey(authKey);
				//갱신된 사용자 정보를 사용자 인증 저장소에 덮어씌움.
				mapUtil.putUserData(account, userData);
				
				response.status = 100;
				res.json(response);
				commonUtil.logPrint('[로그아웃] 로그아웃 성공, ' + account);
			}
			//사용자의 정보에 일치하는 인증키가 존재하지 않음. -> 인증키가 유효하지 않음.
			else {
				response.status = 103;
				res.json(response);
				commonUtil.logPrint('[로그아웃] 올바르지 않은 인증키, ' + account);
			}
		}
		//사용자의 정보가 존재하지 않음. -> 인증키가 유효하지 않음.
		else {
			response.status = 103;
			res.json(response);
			commonUtil.logPrint('[로그아웃] 로그인 정보가 존재하지 않음, ' + account);
		}
	}
	else {
		response.status = 103;
		res.json(response);
		commonUtil.logPrint('[로그아웃] 전달받은 값이 올바르지 않음, ' + account);
	}
};

exports.join = function(req, res) {
	var account = req.body.account;
	var pwd = req.body.pwd;
	
	var response = {};
	
	if (account && pwd) {
		//이미 존재하는 아이디인지 확인한다.
		dbUtil.isExistUserId(account, function(isExist) {
			//존재하는 아이디.
			if (isExist) {
				response.status = 202;
				res.json(response);
				commonUtil.logPrint('[회원가입] 이미 가입된 계정, ' + account);
			}
			//존재하지 않는 아이디.
			else {
				//새로운 회원을 등록한다.
				dbUtil.registryNewUser(account, pwd, function(isSuccess) {
					//회원 가입 성공.
					if (isSuccess) {
						response.status = 200;
						res.json(response);
						commonUtil.logPrint('[회원가입] 회원 가입 성공, ' + account);
					}
					//회원 가입 실패.
					else {
						response.status = 201;
						res.json(response);
						commonUtil.logPrint('[회원가입] 회원 가입 실패, ' + account);
					}
				});
			}
		});
	}
	else {
		response.status = 201;
		res.json(response);
		commonUtil.logPrint('[회원가입] 전달받은 값이 올바르지 않음, ' + account);
	}
};