var mapUtil = require('../private/utils/mapUtil');

// 메인 로그인 페이지
exports.index = function(req, res){
  console.log('index 호출');
  res.render('index');
};

// 회원 가입 페이지
exports.signup = function(req, res) {
  console.log('signup 호출');
  res.render('signup');
};

/*
  Process Status Code
   - 400 : Post 접근
   - 401 : Get 접근
   - 402 : 잘못된 인증키로 접근
   - 403 : 아직 가입하지 않은 사용자가 접근
*/

exports.process_get = function(req, res) {
  console.log('process_get 호출');
  
  var response = {status:401};
  
  res.render('process', response);
  
};

exports.process_post = function(req, res){
  console.log('process_post 호출');
  
  var account = req.body.loginEmail;
  var authKey = req.body.loginAuth;
  
  console.log('account : ' + account + ', auth : ' + authKey);
  
  var response = {};
  var userData = mapUtil.getUserData(account);

  if (account && authKey) {
    //사용자의 정보가 존재함.
	if (userData) {
	  //사용자의 정보에 일치하는 인증키가 존재함.
	  if (userData.isExistAuthKey(authKey)) {
	      response.status = 400;
		  response.data = account;
		}
		//사용자의 정보에 일치하는 인증키가 존재하지 않음.
		else {
		  response.status = 402;
		  response.data = null;
		}
	}
	//사용자의 정보가 존재하지 않음.
	else {
	  response.status = 403;
	  response.data = null;
	}
  }
  // 인증키가 존재하지 않을 경우,
  else {
    response.status = 402;
	response.data = null;
  }
  
  console.log('status 값 : ' + response.status);

  res.render('process', response);
}; 

exports.memo_get = function(req, res) {
  console.log('memo_get 호출');
  
  // 메모 페이지 Get 접근
  var response = {status:501};
  
  res.render('memo', response);
  
};

exports.memo_post = function(req, res) {
  console.log('memo_post 호출');
  
  var account = req.body.reqAccount;
  
  var response = {status:500, data:account};
  
  res.render('memo', response);
  
};















